//
var io = require('socket.io')({transports: ['websocket'],});
io.attach(3000);

// Client connection 
io.on('connection', function(socket){
	
	console.log('Client connected');

	var timerId = setInterval( function(){
		console.log('spin');
		var speed = randomIntFromInterval(10,200);
		io.emit('spin', {speed:speed});
	}, 2000);

	socket.on('disconnect', function() {
		console.log('Disconnect');		
		io.emit('disconnected');

		clearInterval(timerId);
	})
})

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}
