﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class Spin : MonoBehaviour {

	private SocketIOComponent socket;

	public float speed = 10;
	public float tumble = 0;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();
		socket.On("spin", doSpin);
	}

	public void doSpin(SocketIOEvent e)
	{
		//Debug.Log("[SocketIO] Spin received: " + e.name + " " + e.data);
		if (e.data != null) {
			JSONObject s = e.data.GetField("speed");
			this.speed = s.n;
			Debug.Log(s);

			this.tumble = Random.Range (10, 50);
		}			
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.up, this.speed * Time.deltaTime);
		transform.Rotate(Vector3.left, this.tumble * Time.deltaTime);
	}
}
